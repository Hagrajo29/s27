/*
const http = require("http");

//Mock Database

let database = [
{
	"name": "Brandon",
	"email": "brandon@mail.com"
},
{
	"name": "Jobert",
	"email": "jobert@mail.com"
}
]


http.createServer((req, res) =>{
	if(req.url === "/users" && req.method ==="GET"){
		response.writeHead(200, {"Content-Type":"_application/json"});
		res.write(JSON.stringify(database));
		res.end();
	}
}).listen(4000)


console.log("Server is running at localhost: 4000")
*/
const http = require("http");

// Mock Database
let database = [
{
	"name": "Brandon",
	"email": "brandon@mail.com"
},
{
	"name": "Jobert",
	"email": "jobert@mail.com"
}
]

http.createServer((req, res) =>{
	// route for returning all items upon receiving a get req
	if (req.url === "/users" && req.method === "GET") {
		res.writeHead(200, {"Content-Type": "_application/json"});
		// res.write() function is used to print what is inside the parameters as a response
		// Input has to be in a form of string that is why JSON.stringify is used.
		// the data that will be received by the users/client from the sever will be in a form of stringified JSON
		res.write(JSON.stringify(database));
		res.end();
	}
	if (req.url ==="/users" && req.method ==="POST") {
		let requestBody = ""

/*
	data stream - flow/ sequence of data
		data step- data is received from the client and is processed in the stream 				called "data" where the code/staements will be triggered

		end step- only runs after the request has completely been sent once data has already been processed
*/

		req.on("data", function(data){
			//data will be assigned as the value of the requestBody
			requestBody +=data

		})
		req.on("end", function(){
			//check the type of data of requestBody
			requestBody= JSON.parse(requestBody)
		// the server needs an object to arrange information in the database mre efficiently that is why we need JSON.parse
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}
			//adds the newUser into the database
			database.push(newUser);
			console.log(database);


			res.writeHead(200,{"Content-Type": "_application/json"});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}


}).listen(4000)

console.log("Server is running at localhost: 4000")
