/*
const http = require("http");

const port = 4000;

const server = http.createServer((request, response)=>{
	

	if (req.url === "/items" && req.method === "POST") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data retrieved from the database.");
	}
	if (req.url === "/items" && req.method === "PUT") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Update resources.");
	}
	if (req.url === "/items" && req.method === "DELETE") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Delete resources");
	}

});

server.listen(port);

console.log(`Server now running at localhost:${port}`);
*/
const http = require("http");

const port = 4000;

const server = http.createServer( ( req, res ) =>{
	// http method of the incoming requests can be accessed via "req.method"
		// GET method - retrieving/reading information; default method
	if ( req.url === "/items" && req.method === "GET" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Data retrieved from the database");
	}
	/*
		create a "items" url with POST method
		the response should be "Data to be sent to the database" with 200  as status code and plain text as the content type
	*/
	/*
		POST, PUT, and DELETE operation do not work in the browser unlike the Get method. with this, Postman solves the problem by simulating a frontend for the developers to test their codes.
	*/
	if ( req.url === "/items" && req.method === "POST" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Data to be sent to the database");
	}
	if ( req.url === "/items" && req.method === "PUT" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Update resources");
	}
	if ( req.url === "/items" && req.method === "DELETE" ) {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Delete resources");
	}
} );

server.listen(port);

console.log(`Server is running at localhost: ${port}`);
